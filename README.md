Das Projekt heißt "Wer wird Millionär?". 
Der Inhalt des Programmes wird aus fünf Schwierigkeitsstufen bestehen, die aus je drei Fragen eine zufällige auswählt. 
Zusätzlich zu jedder Frage wird es einen 50:50 und einen "Zuschauer"-Joker geben, die zur Lösung der Frage helfen sollen. 
Die größte Schwierigkeit wird sein, aus den drei vorhandenen Fragen eine zufällig auszuwählen und auch die Antworten immer in einer anderen Reihenfolge anzuzeigen. 
Als Lösungsansatz wird eine Funktion geschrieben, die mit RAMDOM sowohl Frage als auch Antwort ausgibt. Eine zweite Funktion wird dann die Lösung auf Richtigkeit überprüfen. 